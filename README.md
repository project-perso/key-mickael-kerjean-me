![badge](https://gitlab.com/project-perso/key-mickael-kerjean-me/badges/master/build.svg)

# What is it?
A fork of a keepass web client: https://github.com/antelle/keeweb
- [original demo](https://keeweb.info/)
- gitlab page is available at: [http://project-perso.gitlab.io/key-mickael-kerjean-me/](http://project-perso.gitlab.io/key-mickael-kerjean-me/)

# How does it works?
Simply fork this repository, it should start a build and create a page at the following url:
```
https://$USERNAME.gitlab.io/$REPOSITORY_NAME
```

# Setup domain
On gitlab:
```
Seetings => page => new domain => link to your domain
```
To access from your domain, simply update the DNS configuration:
```
my_domain CNAME $USERNAME.gitlab.io.
```

